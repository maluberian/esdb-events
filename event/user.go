package event

import "gitlab.com/maluberian/esdb-events/common"

type UserData struct {
	Id        string `json:"uid"`
	FirstName string `json:"first-name"`
	LastName  string `json:"last-name"`
	Email     string `json:"email"`
}

type UserCommand struct {
	Id     string   `json:"id"`
	Action string   `json:"action"`
	UserId string   `json:"user-id"`
	Data   UserData `json:"data"`
}

type UserEventType string

const (
	UserUndefinedEventType UserEventType = ""
	UserAddEventType                     = "add"
	UserModifyEventType                  = "modify"
	UserDeleteEventType                  = "delete"
)

type UserEvent struct {
	Id     string                      `json:"id"`
	Uid    string                      `json:"uid"`
	Type   UserEventType               `json:"type"`
	Fields map[common.UserField]string `json:"fields"`
}

type UserAddEvent struct {
	event UserEvent
}

func (ua UserAddEvent) New() {
	ua.event = UserEvent{Type: UserAddEventType}
}

type UserModifyEvent struct {
	event UserEvent
}

func (um UserModifyEvent) New() {
	um.event = UserEvent{Type: UserModifyEventType}
}

type UserDeleteEvent struct {
	event UserEvent
}

func (ud UserDeleteEvent) New() {
	ud.event = UserEvent{Type: UserDeleteEventType}
}
