package event

type Subscription struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Emails []string `json:"emails"`
}
