package common

type UserField string

const (
	UserFirstNameField UserField = "fname"
	UserLastNameField            = "lname"
	UserEmailField               = "email"
	UserCreatedField             = "created-dt"
	UserModifiedField            = "updated-dt"
	UserDeletedField             = "deleted-dt"
)
