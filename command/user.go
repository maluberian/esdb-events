package command

import "gitlab.com/maluberian/esdb-events/common"

type UserCommandAction string

const (
	UserUndefinedCommandType UserCommandAction = ""
	UserAddCommandType                         = "add"
	UserModifyCommandType                      = "modify"
	UserDeleteCommandType                      = "delete"
)

// UserCommand represents commands that can be processed against a
// user. These commands will cause the domain e
type UserCommand struct {
	Id     string                      `json:"id"`
	Action UserCommandAction           `json:"action"`
	UserId string                      `json:"user-id"`
	Data   map[common.UserField]string `json:"data""`
}
