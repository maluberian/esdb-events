package command

type SubscriptionAction string

const (
	SubscribeType   SubscriptionAction = "subscribe"
	UnsubscribeType                    = "unsubscribe"
)

type SubscriptionCommand struct {
	Id             string             `json:"id""`
	Action         SubscriptionAction `json:"subscription-action"`
	SubscriptionId string             `json:"subscription-id"`
	UserId         string             `json:"user-id"`
}
